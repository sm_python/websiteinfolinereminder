var countDownDate = new Date("Feb 4, 2018 15:37:25").getTime();
var countdownfunction = setInterval(function () {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    var en = 'Reminder! Your site will be blocked after: ';
    var ru = ' НАПОМИНАНИЕ об оплате услуги поддержки сайта. Ваш сайт будет заблокирован через: ';
    var ua = ' НАГАДУВАННЯ про оплату послуги підтримки сайту. Ваш сайт буде заблоковано через: ';
    var warning = '&#9888; &nbsp;';
    var phone = ' &emsp; &#9743; ';
    var bphone = ' &emsp; &#9742; ';
    var space = '  &emsp; &#9774;  &emsp;';
    var star = ' &emsp;  &#9734; &emsp; ';
    var bstar = ' &emsp;  &#9733; &emsp; ';
    document.getElementById("date").innerHTML = warning + ru + days + "д " + hours + "ч " + minutes + "м " + seconds + "с " + space + ua + days + "д " + hours + "г " + minutes + "хв " + seconds + "с " + bstar + en + days + "d " + hours + "h " + minutes + "m " + seconds + "s " + bphone ;

    if (distance < 0) {
        clearInterval(countdownfunction);
        document.getElementById("date").innerHTML = "EXPIRED";
    }
}, 1000);